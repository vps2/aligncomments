QMAKE_CXXFLAGS += -std=c++11

DEFINES += ALIGNCOMMENTS_LIBRARY

# AlignComments files

SOURCES += aligncommentsplugin.cpp

HEADERS += aligncommentsplugin.h \
        aligncomments_global.h \
		  aligncommentsconstants.h \
		  plugin_utilities.h

# Qt Creator linking

## set the QTC_SOURCE environment variable to override the setting here
QTCREATOR_SOURCES = $$(QTC_SOURCE)
isEmpty(QTCREATOR_SOURCES):QTCREATOR_SOURCES=C:/Programs/Qt/qt-creator-2.8.1-src

## set the QTC_BUILD environment variable to override the setting here
IDE_BUILD_TREE = $$(QTC_BUILD)
CONFIG(debug, debug|release) {
	isEmpty(IDE_BUILD_TREE):IDE_BUILD_TREE=C:/Programs/Qt/qt-creator-build/debug
} else {
	isEmpty(IDE_BUILD_TREE):IDE_BUILD_TREE=C:/Programs/Qt/qt-creator-build/release
}


## uncomment to build plugin into user config directory
## <localappdata>/plugins/<ideversion>
##    where <localappdata> is e.g.
##    "%LOCALAPPDATA%\QtProject\qtcreator" on Windows Vista and later
##    "$XDG_DATA_HOME/data/QtProject/qtcreator" or "~/.local/share/data/QtProject/qtcreator" on Linux
##    "~/Library/Application Support/QtProject/Qt Creator" on Mac
# USE_USER_DESTDIR = yes

PROVIDER = VPS

include($$QTCREATOR_SOURCES/src/qtcreatorplugin.pri)

