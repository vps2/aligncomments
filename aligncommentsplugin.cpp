#include "aligncommentsplugin.h"
#include "aligncommentsconstants.h"
#include "plugin_utilities.h"

#include <coreplugin/icore.h>
#include <coreplugin/icontext.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/coreconstants.h>

//------------------------------------------------------------------------------

#include <coreplugin/editormanager/ieditor.h>
#include <qmljseditor/qmljseditorconstants.h>
#include <cppeditor/cppeditorconstants.h>
#include <coreplugin/editormanager/editormanager.h>
#include <texteditor/basetexteditor.h>
#include <texteditor/tabsettings.h>
#include <QTextCursor>
#include <QTextDocumentFragment>

//------------------------------------------------------------------------------

#include <QAction>
#include <QMainWindow>
#include <QMenu>

#include <QtPlugin>

using namespace AlignComments::Internal;

AlignCommentsPlugin::AlignCommentsPlugin()
{
	// Create your members
}

AlignCommentsPlugin::~AlignCommentsPlugin()
{
	// Unregister objects from the plugin manager's object pool
	// Delete members
}

bool AlignCommentsPlugin::initialize(const QStringList &arguments, QString *errorString)
{
	// Register objects in the plugin manager's object pool
	// Load settings
	// Add actions to menus
	// Connect to other plugins' signals
	// In the initialize method, a plugin can be sure that the plugins it
	// depends on have initialized their members.

	Q_UNUSED(arguments)
	Q_UNUSED(errorString)

	m_action = new QAction(tr("Align comments"), this);
	m_action->setEnabled(false);

	Core::Command *cmd = Core::ActionManager::registerAction(m_action, Constants::ACTION_ID,
																				Core::Context(Core::Constants::C_GLOBAL));
	cmd->setDefaultKeySequence(QKeySequence(tr("Ctrl+Shift+A")));

	Core::ActionManager::actionContainer(Core::Constants::M_EDIT_ADVANCED)->addAction(cmd);

	createConnections();

	return true;
}

void AlignCommentsPlugin::extensionsInitialized()
{
	// Retrieve objects from the plugin manager's object pool
	// In the extensionsInitialized method, a plugin can be sure that all
	// plugins that depend on it are completely initialized.
}

ExtensionSystem::IPlugin::ShutdownFlag AlignCommentsPlugin::aboutToShutdown()
{
	// Save settings
	// Disconnect from signals that are not needed during shutdown
	// Hide UI (if you add UI that is not in the main window directly)
	return SynchronousShutdown;
}

void AlignCommentsPlugin::createConnections()
{
	connect(m_action, SIGNAL(triggered()), this, SLOT(triggerAction()));
	connect(Core::EditorManager::instance(), SIGNAL(currentEditorChanged(Core::IEditor *)),
			  this, SLOT(editorChanged(Core::IEditor *)));
}

void AlignCommentsPlugin::moveCursorToStartOfBlock(QTextCursor &cursor)
{
	if(cursor.atBlockStart())
	{
		cursor.movePosition(QTextCursor::StartOfLine, QTextCursor::KeepAnchor);
	}
	else
	{
		QTextCursor oldCursor = cursor;
		cursor.setPosition(oldCursor.selectionEnd());
		cursor.setPosition(oldCursor.selectionStart(), QTextCursor::KeepAnchor);
		cursor.movePosition(QTextCursor::StartOfLine, QTextCursor::KeepAnchor);
	}
}

void AlignCommentsPlugin::triggerAction()
{
	using TxtEdWidget = TextEditor::BaseTextEditorWidget;

	TxtEdWidget *textEditorWidget = static_cast<TxtEdWidget *>(m_editor->widget());

	QTextCursor &&cursor = textEditorWidget->textCursor();

	moveCursorToStartOfBlock(cursor);

	QTextDocumentFragment &&formattedTextFragment = cursor.selection();
	QString &&text = formattedTextFragment.toPlainText();
	if(text.isEmpty())
	{
		return;
	}

	const TextEditor::TabSettings &tabSettings = textEditorWidget->tabSettings();
	int tabSize = tabSettings.m_tabSize;

	if(PluginUtilities::alignCommentsInText(text, tabSize))
	{
		cursor.insertText(text);
	}
}

void AlignCommentsPlugin::editorChanged(Core::IEditor *editor)
{
	m_editor = editor;

	bool enableAction = false;

	if(m_editor)
	{
		if(m_editor->context().contains(CppEditor::Constants::CPPEDITOR_ID) ||
			m_editor->context().contains(QmlJSEditor::Constants::C_QMLJSEDITOR_ID))
		{
			enableAction = true;
		}
	}

	m_action->setEnabled(enableAction);
}

Q_EXPORT_PLUGIN2(AlignComments, AlignCommentsPlugin)

