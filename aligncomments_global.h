#ifndef ALIGNCOMMENTS_GLOBAL_H
#define ALIGNCOMMENTS_GLOBAL_H

#include <QtGlobal>

#if defined(ALIGNCOMMENTS_LIBRARY)
#  define ALIGNCOMMENTSSHARED_EXPORT Q_DECL_EXPORT
#else
#  define ALIGNCOMMENTSSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // ALIGNCOMMENTS_GLOBAL_H

