#ifndef PLUGIN_UTILITIES_H
#define PLUGIN_UTILITIES_H

#include <QString>
#include <QStringList>
#include <QMap>
#include <QRegExp>

#include <algorithm>
#include <iterator>
#include <functional>

namespace PluginUtilities
{
	const QString NEW_LINE = QLatin1String("\n");
	const QString COMMENTS_REG_EXPR = QLatin1String("//.*$");
	const int WRONG_COMMENTS_POSITION = -1;

	bool isLineWithComment(const QString &str, int &pos)
	{
		QRegExp reg(COMMENTS_REG_EXPR);
		pos = reg.indexIn(str);

		return pos > WRONG_COMMENTS_POSITION;
	}

	QStringList splitByDelimiter(const QString &text, const QRegExp &delim)
	{
		return text.split(delim);
	}

	QString stringFromStringList(const QStringList &rowsList)
	{
		QString editedText;
		QStringList::ConstIterator endIt = rowsList.constEnd();
		for(QStringList::ConstIterator it = rowsList.constBegin(); it != endIt; ++it)
		{
			editedText.append(*it);
			if(std::distance(it, endIt) > 1)
			{
				editedText.append(NEW_LINE);
			}
		}

		return editedText;
	}

	/*!
	 * \~russian
	 * Выравнивает комментарии в конце строк кода в выделеном блоке.
	 * @fn bool alignCommentsInText(QString &text, const int tabSize)
	 * @param text текст, в котором необходимо выровнять комментарии
	 * @param tabSize размер табуляции
	 *
	 * \@warning все символы табуляции заменяются пробелами.
	 */
	//TODO: разбить её на более мелкие функции.
	bool alignCommentsInText(QString &text, const int tabSize)
	{		
		QString &&spaces = QString(QLatin1Char(' ')).repeated(tabSize);
		text.replace(QRegExp(QLatin1String("\t")), spaces);

		QStringList &&rowsList = splitByDelimiter(text, QRegExp(NEW_LINE));

		int maxPos = WRONG_COMMENTS_POSITION;
		QMap<int, int> rowsWithComment;	
		int index = 0;	//обычная инициализация переменной нулём

		foreach(const QString &row, rowsList)
		{
			int pos = WRONG_COMMENTS_POSITION;
			if(isLineWithComment(row, pos))
			{
				rowsWithComment.insert(index, pos);

				if(pos > maxPos)
				{
					maxPos = pos;
				}
			}

			++index;
		}

		using QMapIterator = QMap<int, int>::Iterator;

		bool modified = false;
		QMapIterator endIt = rowsWithComment.end();

		for(QMapIterator it = rowsWithComment.begin(); it != endIt; ++it)
		{
			const int OFFCET = qAbs(maxPos - it.value());

			if(OFFCET > 0)
			{
				QString row = rowsList.at(it.key());
				QString &&spaces = QString(QLatin1Char(' ')).repeated(OFFCET);
				row.insert(it.value(), spaces);
				rowsList.replace(it.key(), row);
				modified = true;
			}
		}

		QString &&editedText = stringFromStringList(rowsList);

		text.swap(editedText);

		return modified;
	}

}

#endif // PLUGIN_UTILITIES_H
