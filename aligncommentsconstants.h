#ifndef ALIGNCOMMENTSCONSTANTS_H
#define ALIGNCOMMENTSCONSTANTS_H

namespace AlignComments
{
	namespace Constants
	{
		const char ACTION_ID[] = "AlignComments.Action";
	} // namespace AlignComments
} // namespace Constants

#endif // ALIGNCOMMENTSCONSTANTS_H

