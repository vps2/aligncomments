# AlignComments #

A plugin for Qt Creator, that implements the ability to align comments at the end of lines of code in an allocated block.
The plugin has been tested with qt creator 2.8.1 and library qt-4.8.6.
Source code of the plugin uses the syntax of C++11.

### Setup before building ###

Prior to build a plug-in, in project file "aligncomments.pro" you must specify the path to the Qt Creator source, and the path to the folder where it is installed. To do this, you must change the value of variables: QTCREATOR_SOURCES and IDE_BUILD_TREE.

### Usage ###

If the build succeeds, then in the window of Installed Plugins, under Utilites you will see the plugin under the name "AlignComments". After turning it on and restart the IDE, will appears menu "Edit -> Advanced -> Align comments (Ctrl+Shift+A)".
After selecting the block of code with is not aligned comments and selecting menu item - the comments will be aligned according to the farthest.
# 
#
#
**Note**:  this plugin may not be built in Qt Creator 3 and later, because there has changed API.