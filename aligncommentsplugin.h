#ifndef ALIGNCOMMENTS_H
#define ALIGNCOMMENTS_H

#include "aligncomments_global.h"

#include <extensionsystem/iplugin.h>

namespace Core
{
	class IEditor;
}

class QAction;
class QTextCursor;

namespace AlignComments
{
	namespace Internal
	{

		class AlignCommentsPlugin : public ExtensionSystem::IPlugin
		{
				Q_OBJECT
				Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QtCreatorPlugin" FILE "AlignComments.json")

			public:
				AlignCommentsPlugin();
				~AlignCommentsPlugin();

				bool initialize(const QStringList &arguments, QString *errorString);
				void extensionsInitialized();
				ShutdownFlag aboutToShutdown();

			private slots:
				void triggerAction();
				void editorChanged(Core::IEditor *editor);

			private:
				void createConnections();
				void moveCursorToStartOfBlock(QTextCursor &cursor);

				Core::IEditor *m_editor = nullptr;
				QAction *m_action = nullptr;
		};

	} // namespace Internal
} // namespace AlignComments

#endif // ALIGNCOMMENTS_H

